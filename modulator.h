#ifndef _MODULATOR_H_
#define _MODULATOR_H_

#define CONNECT(sender, signal, handler, action) \
{ \
    auto sender_ptr = (sender); \
    auto handler_ptr = (handler); \
    sender_ptr->subscr(signal, [handler_ptr]{ handler_ptr->action; }); \
}

#include "signals.h"

#include <unordered_map>
#include <vector>
#include <functional>

namespace modulator_n {

class modulator_ {
public:
    using slot_ = std::function<void()>;

    virtual ~modulator_() = 0;
    void subscr(signals_n::signals_ signal, slot_ slot);

protected:
    void emit(signals_n::signals_ signal) const;

protected:
    std::unordered_map<signals_n::signals_, std::vector<slot_>> _emit_map;

};

} // modulator_n

#endif // _MODULATOR_H_
