#include "modulator.h"

#include <iostream>

class responser_ : public modulator_n::modulator_ {
public:
    void action() {
        std::cout << "responser_: Was action!\n";
    }

} responser;

class envoker_ : public modulator_n::modulator_ {
public:
    void tmp() {
        std::cout << "envoker_ emited TMP\n";
        emit(signals_n::TMP);
    }
} envoker;

class internal_ : public modulator_n::modulator_ {
public:
    void action() {
        std::cout << "internal_: Was action!\n";
    }

};

class external_ : public modulator_n::modulator_ {
public:
    external_() {
        CONNECT(this, signals_n::TMP, &internal, action());
    }
    void tmp() {
        std::cout << "external_ emited TMP\n";
        emit(signals_n::TMP);
    }

private:
    internal_ internal;

} external;

class internal2_ : public modulator_n::modulator_ {
public:
    void tmp() {
        std::cout << "internal2_ emited TMP\n";
        emit(signals_n::TMP);
    }
};

class external2_ : public modulator_n::modulator_ {
public:
    external2_() {
        CONNECT(&internal2, signals_n::TMP, this, action());
    }

    void tmp() {
        internal2.tmp();
    }

private:
    void action() {
        std::cout << "external2: Was action!\n";
    }

private:
    internal2_ internal2;

} external2;

int main() {
    CONNECT(&envoker, signals_n::TMP, &responser, action());
    envoker.tmp();

    external.tmp();

    external2.tmp();

    return 0;
}
