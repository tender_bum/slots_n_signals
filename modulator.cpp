#include "modulator.h"

namespace modulator_n {

modulator_::~modulator_() {
}

void modulator_::subscr(signals_n::signals_ signal, slot_ slot) {
    _emit_map[signal].push_back(slot);
}

void modulator_::emit(signals_n::signals_ signal) const {
    auto slot_list = _emit_map.find(signal);
    if (slot_list == std::end(_emit_map))
        return;
    for (slot_ slot : slot_list->second)
        slot();
}

} // modulator_n
