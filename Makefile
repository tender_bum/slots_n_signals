CC=clang++
CXXFLAGS=-std=c++14
OBJ=main.o modulator.o
DEP=modulator.h signals.h
BIN=main

exec: $(BIN)
	./$^

$(BIN): $(OBJ)
	$(CC) $(CXXFLAGS) -o $@ $^

%.o: %.cpp $(DEP)
	$(CC) $(CXXFLAGS) -c -o $@ $<

clear:
	rm $(OBJ) $(BIN)
